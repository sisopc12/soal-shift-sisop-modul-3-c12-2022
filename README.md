# soal-shift-sisop-modul-3-C12-2022

# SOAL 1
<hr>

    void create_directory(){
        // bikin direktori quote
        char *argv_quote[] = {"mkdir", "-p", "`pwd`/quote", NULL};
        //bikin direktori music
        char *argv_music[] = {"mkdir", "-p", "`pwd`/music", NULL};
        // `pwd` maksudnya ambil alamat project
        child_id = fork();
        int flag_status;
        //execute command
        if(child_id == 0){
            execv("/bin/mkdir", argv_quote);
        }else{
            while(wait(&flag_status) > 0);
            execv("/bin/mkdir", argv_music);
        }
    }

<p>
Fungsi berikut digunakan untuk create directory. Membuat folder untuk quote dan music. dengan command execute kalau child_id ==0 maka execv quote kalau tidak execv music.
</p>

<hr>

    int errors;
    int i = 0;
    while(i < 2){
        errors= pthread_create(&(thread_id[i]), NULL, &thread_unzip, NULL);
        if(errors!= 0){
            printf("unzip failed for file : [%s]\n", strerror(errors));
        }else{
            printf("success creating thread\n");
        }
        i++;

<hr>

    void *thread_unzip(){
        printf("Melakukan unzip file\n");
        //argv command unzip quote
        char *argv_quote[] = {"unzip", "-q", "quote.zip", NULL};
        //argv command unzip music
        char *argv_music[] = {"unzip", "-q", "music.zip", NULL};

        unsigned long long i = 0;
        pthread_t id = pthread_self();
        //execute command pakai execv
        if(pthread_equal(id, thread_id[0])){
            child_id = fork();
            if(child_id == 0)
                execv("/bin/unzip", argv_quote);
        }else if(pthread_equal(id, thread_id[1])){
            child_id = fork();
            if(child_id == 0)
                execv("/bin/unzip", argv_music);
        }
        return NULL;
    }

<p>
Berikut merupakan fungsi yang seperti modul untuk mexecture program dengan menggunakan thread, ykni kalau di sini akan me-unzip file quote dan music.
</p>

    int main(){

        int errors;
        printf("Soal 1 Praktikum Sisop Modul 3\n");
        int i = 0;
        while(i < 2){
            errors= pthread_create(&(thread_id[i]), NULL, &thread_unzip, NULL);
            if(errors!= 0){
                printf("unzip failed for file : [%s]\n", strerror(errors));
            }else{
                printf("success creating thread\n");
            }
            i++;
        }
        //pemanggilan fungsi membuat folder
        create_directory();
        // join thread 
        pthread_join(thread_id[0], NULL);
        pthread_join(thread_id[1], NULL);
        exit(0);
        
        return 0;
    }
<p>
Dengan fungsi main berguna ada pemanggilan fungsi create directory dan threadnya.
</p>

![1](/uploads/dacb099c96a6c561a9467caa833c3654/1.PNG)

<p>
Berikut merupakan hasil exekusi unzip dan create foldernya.
</p>
